/* copyleft (C) GPL3 {{{2
 * Filename:	printw.c
 *
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:  Tue Jan  8 21:12:02 2008
 * }}}*/
/*header files	{{{1*/
#include <ncurses.h>
#include <string.h>
#include "debug.h"
/*}}}*/

/*declaration		{{{1*/
/*}}}*/

/*functions		{{{1*/
int main(int argc, char *argv[])
{
	char mesg[] = "Just a string";
	int row, col;

	initscr();
	getmaxyx(stdscr, row, col);
	mvprintw(row/2, (col - strlen(mesg))/2, "%s", mesg);
	mvprintw(row - 2, 0, "This screen has %d rows and %d columns\n", row, col);
	printw("Try resizing your window(if possible) and then run this program again");
	refresh();
	getch();
	endwin();

	return 0;
}

/* vim:fdm=marker:ts=8:ft=c:norl:fdl=1:
 * }}}*/

