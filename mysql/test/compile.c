/* copyleft (C) GPL3 {{{2
 * Filename:	compile.c
 *
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:  Wed Dec 26 14:38:41 2007
 * }}}*/
/*header files	{{{1*/
#include <mysql.h>
#include "debug.h"
#include "defines.h"
/*}}}*/

/*declaration		{{{1*/
static void app_init(void);
static void app_exit(void);

static void mysql(void);
static void mysql_show_num(MYSQL *mysql);
static void mysql_show_data(MYSQL *mysql);

static int row, col;
/*}}}*/

/*functions		{{{1*/
int main(int argc, char *argv[])
{
	app_init();
	mysql();
	app_exit();
	return 0;
}

static void app_init(void)
{
	initscr();
	getmaxyx(stdscr, row, col);
	mvseep(row - 1, 0, "win init ok, row = %d, col = %d\n", row, col);
	refresh();
}

static void app_exit(void)
{
	int ch;

	refresh();
	for (;;) {
		ch = getch();
		if (ch == KEY_RESIZE) {
			clear();
			app_init();
			mysql();
			refresh();
			continue;
		}
		break; /* will exit */
	}
	endwin();
}

static void mysql(void)
{
	MYSQL mysql;

	mysql_init(&mysql);
	/* mysql_options(&mysql, MYSQL_READ_DEFAULT_GROUP, "your_prog_name"); */

	if (!mysql_real_connect(&mysql, DB_HOST, DB_USER, DB_PWD,
		DB_DATABASE, 0, NULL, 0)) {
		mvseep(row - 2, 0, "Failed to connect to database: Error: %s\n",
			mysql_error(&mysql));
	} else {
		mvseep(row - 2, 0, "success connect to database\n");
		refresh();
		/* sleep(2); */
		mysql_show_num(&mysql);
		mysql_show_data(&mysql);
	}
}

static void mysql_show_num(MYSQL *mysql)
{
	MYSQL_RES *result;
	unsigned int num_fields = 0;
	unsigned int num_rows = 0;
	char *query_string = "select * from stats";

	if (mysql_query(mysql, query_string)) {

	} else {
		result = mysql_store_result(mysql);
		if (result) {
			num_fields = mysql_num_fields(result);
			/* num_rows = mysql_affected_rows(mysql); */
			num_rows = mysql_num_rows(result);
		} else{
			if (mysql_field_count(mysql) == 0) {
				num_rows = mysql_affected_rows(mysql);
			} else {
				fprintf(stderr, "Error: %s\n", mysql_error(mysql));
			}
		}
	}
	mvseep(0, 0, "data fileds = %d, rows = %d\n", num_fields, num_rows);
	mysql_free_result(result);
}

static void mysql_show_data(MYSQL *mysql)
{
	int i = 0;
	MYSQL_ROW record;
	MYSQL_RES *result;

	mysql_query(mysql, "SELECT * FROM stats");
	result = mysql_store_result(mysql);
	seep("\n");
	while (((i ++) < row - 6) && (record = mysql_fetch_row(result))) {
		seep("%s - %s - %s\n", record[0], record[2], record[3]);
	}

	mysql_free_result(result);
}

/* vim:fdm=marker:ts=8:ft=c:norl:fdl=1:
 * }}}*/

