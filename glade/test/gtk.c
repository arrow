#include <gtk/gtk.h>
#include <glade/glade.h>

void some_handler(GtkWidget * widget)
{
	/* a handler referenced by the glade file.  Must not be static
	 * so that it appears in the global symbol table. */
}

int main(int argc, char **argv)
{
	GladeXML *xml;
	GtkWidget *widget;

	gtk_init(&argc, &argv);
	xml = glade_xml_new("xml.glade", NULL, NULL);

	/* get a widget (useful if you want to change something) */
	widget = glade_xml_get_widget(xml, "GtkWindow");

	/* connect signal handlers */
	glade_xml_signal_autoconnect(xml);

	gtk_main();

	return 0;
}
