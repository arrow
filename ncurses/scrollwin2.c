#include <stdlib.h>
#include <ncurses.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
static void finish(int sig);
WINDOW *scrwin, *boxwin;
int main(int argc, char *argv[])
{
	FILE *fp;
	int i, j;

	fp = fopen("zz.txt", "w+");
	if (fp == NULL) {
		printf("open file fail\n");
		return 0;
	}
	initscr();
	cbreak();
	noecho();
	nonl();
	scrwin = newwin(10, 40, LINES / 2 - 6, COLS / 2 - 25);
	boxwin = newwin(12, 42, LINES / 2 - 7, COLS / 2 - 26);
	scrollok(scrwin, 1);
	box(boxwin, '|', '-');
	refresh();
	wrefresh(boxwin);
	signal(SIGINT, finish);
	signal(SIGQUIT, finish);
	for (i = 0;; i++) {
		if (i % 20 == 0)
			if (getchar() == 'q')
				return 0;
		sleep(1);
		wprintw(scrwin, "the string is %d\n", j = i % 9);
		fprintf(fp, "the string is %d\n", j);
		wrefresh(scrwin);

	}
	fclose(fp);
	return 0;
}
void finish(int sig)
{
	endwin();
	exit(0);
}
