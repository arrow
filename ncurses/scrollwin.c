#include <stdlib.h>
#include <ncurses.h>
#include <signal.h>
#include <unistd.h>
static void finish(int sig);
WINDOW *scrwin, *boxwin;
int main(int argc, char *argv[])
{
	int i;

	initscr();
	cbreak();
	noecho();
	nonl();
	scrwin = newwin(10, 40, LINES / 2 - 6, COLS / 2 - 25);
	boxwin = newwin(12, 42, LINES / 2 - 7, COLS / 2 - 26);
	scrollok(scrwin, 1);
	box(boxwin, '|', '-');
	refresh();
	wrefresh(boxwin);
	signal(SIGINT, finish);
	signal(SIGQUIT, finish);
	for (i = 0;; i++) {
		if (i % 20 == 0)
			sleep(1);
		wprintw(scrwin, "the string is %d\n", i % 9);
		wrefresh(scrwin);
	}
	return 0;
}
static void finish(int sig)
{
	endwin();
	exit(0);
}
