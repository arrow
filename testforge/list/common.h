/*
 * Filename:	common.h	
 * Created at:	Mon 20 Aug 2007 07:13:20 PM CST
 *******************************************************************************/
#ifndef _R_COMMON_H_
#define _R_COMMON_H_

#include <linux/list.h>
#define sbup printk
struct arrow_t {
	char name[16];
	int (*fun)(int val);
	struct list_head list;
};

int test_unregister(struct arrow_t *del);
int test_register(struct arrow_t *new);

#endif
