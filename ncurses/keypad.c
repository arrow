/* copyleft (C) GPL3 {{{2
 * Filename:	keypad.c
 *
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:  Tue Jan  8 20:56:02 2008
 * }}}*/
/*header files	{{{1*/
#include <ncurses.h>
#include "debug.h"
/*}}}*/

/*declaration		{{{1*/
/*}}}*/

/*functions		{{{1*/
int main(int argc, char *argv[])
{
	int ch;

	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();
	printw("Type any character to see it in bold\n");
	ch = getch();
	if (ch == KEY_F(1)) {
		printw("F1 key pressed");
	} else {
		printw("The pressed key is ");
		attron(A_BOLD);
		printw("%c\n", ch);
		attroff(A_BOLD);
	}
	refresh();
	getch();
	endwin();

	return 0;
}

/* vim:fdm=marker:ts=8:ft=c:norl:fdl=1:
 * }}}*/

