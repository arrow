colorscheme default
set ch=1
hi IncSearch term=reverse cterm=reverse gui=reverse
hi Search term=reverse ctermbg=darkmagenta ctermfg=white guibg=darkmagenta guifg=white
hi Cursor gui=reverse guifg=darkcyan guibg=white
