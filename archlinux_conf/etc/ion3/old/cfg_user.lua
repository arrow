-- auto start setting
os.execute('arxvt &')
os.execute('arrow-ion-restart')

defmenu("user", {
    submenu("terminal",		"u_term"),
    submenu("net",		"u_net"),
    submenu("edit",		"u_edit"),
    submenu("media",		"u_media"),
    submenu("game",		"u_game"),
    submenu("system",		"u_man"),
    submenu("emulate",		"u_emu"),
    submenu("compress",		"u_comp"),
})

defmenu("u_term", {
    menuentry("arxvt",		"os.execute('arxvt &')"),
    menuentry("pcmanfm",	"os.execute('pcmanfm &')"),
    menuentry("tuxcmd",		"os.execute('tuxcmd &')"),
})
defmenu("u_net", {
    menuentry("firefox",	"os.execute('firefox &')"),
    menuentry("mutt",		"os.execute('mutt &')"),
    menuentry("gpscar",		"os.execute('gpscar &')"),
    menuentry("fetchmail",	"os.execute('fetchmail &')"),
})
defmenu("u_edit", {
    menuentry("gvim",		"os.execute('gvim &')"),
    menuentry("soffice",	"os.execute('soffice &')"),
    menuentry("abiword",	"os.execute('abiword &')"),
})
defmenu("u_media", {
    menuentry("xawtv",		"os.execute('xawtv &')"),
    menuentry("xvidcap",	"os.execute('xvidcap &')"),
    menuentry("recordmydesktop","os.execute('recordmydesktop &')"),
    menuentry("camera_play",	"os.execute('mplayer -fps 25 tv:// &')"),
    menuentry("picture_show",	"os.execute('gthumb &')"),
    menuentry("audio_make",	"os.execute('rosegarden &')"),
})
defmenu("u_game", {
    menuentry("tremulous",	"os.execute('tremulous &')"),
    menuentry("wesnoth",	"os.execute('wesnoth &')"),
})
defmenu("u_man", {
    menuentry("lxrandr",	"os.execute('lxrandr &')"),
})
defmenu("u_emu", {
    menuentry("virtualbox",	"os.execute('VirtualBox &')"),
    menuentry("qemu",		"os.execute('qemu &')"),
    menuentry("dosemu",		"os.execute('dosemu &')"),
})
defmenu("u_comp", {
    menuentry("unrar",		"os.execute('unrar &')"),
    menuentry("tar",		"os.execute('tar &')"),
    menuentry("cabextract",	"os.execute('cabextract &')"),
})

-- user key binding
defbindings("WScreen", {
    kpress("XF86AudioRaiseVolume", "os.execute('amixer set Master 4%+')"),
    kpress("XF86AudioLowerVolume", "os.execute('amixer set Master 4%-')"),
    kpress("XF86ModeLock", "os.execute('xlock')"),
})

dopath("query_url")

--[[dopath("tabmenu");
ioncore.defbindings("WFrame.toplevel", {
   submap(META.."K", {
       kpress("N", "tabmenu.tabmenu(_, _sub, 'next')"),
       kpress("P", "tabmenu.tabmenu(_, _sub, 'prev')"),
   })
})]]


-- download
-- dopath("");
-- dopath("statusd_laptopstatus");
--dopath("vim-bindings");
