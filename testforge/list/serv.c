/* copyleft (C) GPL3 {{{2
 * Filename:	serv.c
 *
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:  Mon 20 Aug 2007 07:05:37 PM CST
 * }}}*/
/*header files	{{{1*/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/list.h>
#include <asm/semaphore.h>
#include <linux/timer.h>
#include "common.h"
/*}}}*/

/*declaration		{{{1*/
LIST_HEAD(list);
struct timer_list timer;
/*DECLARE_MUTEX(sem);*/
/*}}}*/

/*functions		{{{1*/
void loop(unsigned long data)
{
	struct arrow_t *tp;

	timer.expires = jiffies + HZ;
	timer.data = ++ data;
	sbup("looping data = %ld\n", data);
/*        down(&sem);*/
	list_for_each_entry(tp, &list, list) {
		sbup("List name:%s\n", tp->name);
		tp->fun(data);
	}
/*        up(&sem);*/
	add_timer(&timer);
}

int __init init_serv(void)
{
	init_timer(&timer);
	timer.expires = jiffies + HZ;
	timer.function = loop;
	add_timer(&timer);
	return 0;
}

void __exit exit_serv(void)
{
	del_timer(&timer);
}

int test_register(struct arrow_t *new)
{
/*        down(&sem);*/
	list_add(&new->list, &list);
/*        up(&sem);*/
	return 0;
}
EXPORT_SYMBOL(test_register);

int test_unregister(struct arrow_t *del)
{
/*        down(&sem);*/
	list_del(&del->list);
/*        up(&sem);*/
	return 0;
}
EXPORT_SYMBOL(test_unregister);

module_init(init_serv);
module_exit(exit_serv);

/* vim:fdm=marker:ts=8:ft=c:norl:fdl=1:
 * }}}*/

