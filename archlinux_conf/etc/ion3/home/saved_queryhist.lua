-- This file has been generated by Ion. Do not edit.
return {
    [1] = "menu.mainmenu:session/restart",
    [2] = "menu.mainmenu:session/save",
    [3] = "run:firefox",
    [4] = "default:www.google.com",
    [5] = "workspacename:WGroupWS",
    [6] = "workspacename:*scratchws*",
    [7] = "menu.mainmenu:session/exit",
    [8] = "windowname:Google - Vimperator",
    [9] = "run:arxvt",
}

