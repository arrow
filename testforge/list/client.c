/* copyleft (C) GPL3 {{{2
 * Filename:	serv.c
 *
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:  Mon 20 Aug 2007 07:05:37 PM CST
 * }}}*/
/*header files	{{{1*/
#include <linux/kernel.h>
#include <linux/module.h>
#include "common.h"
/*}}}*/

/*declaration		{{{1*/
/*}}}*/

/*functions		{{{1*/
int client(int val)
{
	sbup("I'm in client, val = %d\n", val);
	return 0;
}

struct arrow_t _client = {
	.name = "test ok",
	.fun = client,
};

int __init init_client(void)
{
	test_register(&_client);
	return 0;
}

void __exit exit_client(void)
{
	test_unregister(&_client);
}

module_init(init_client);
module_exit(exit_client);

/* vim:fdm=marker:ts=8:ft=c:norl:fdl=1:
 * }}}*/

