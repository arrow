/*
 * Filename:	debug.h	
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:	Wed Dec 26 14:38:57 2007
 *******************************************************************************/

#ifndef _DEBUG_H_

#include <ncurses.h>
#define seep printw
#define wseep wprintw
#define mvwseep mvwprintw
#define mvseep mvprintw

#endif
