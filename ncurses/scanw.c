/* copyleft (C) GPL3 {{{2
 * Filename:	scanw.c
 *
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:  Tue Jan  8 21:19:42 2008
 * }}}*/
/*header files	{{{1*/
#include <ncurses.h>
#include <string.h>
#include "debug.h"
/*}}}*/

/*declaration		{{{1*/
/*}}}*/

/*functions		{{{1*/
int main(int argc, char *argv[])
{
	char mesg[] = "Enter a string:";
	char str[80];
	int row, col;

	initscr();
	getmaxyx(stdscr, row, col);
	mvprintw(row/2, (col - strlen(mesg))/2, "%s", mesg);
	getstr(str);
	mvprintw(LINES - 2, 0, "You Entered: %s", str);
	getch();
	endwin();

	return 0;
}

/* vim:fdm=marker:ts=8:ft=c:norl:fdl=1:
 * }}}*/

