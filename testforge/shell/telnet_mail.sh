#!/bin/sh -x

if (( $# != 1 )); then
				echo "usage: $0 <mail-host>"
				exit 1
fi

host=$1

i=infile
o=outfile

rm -rf $i $o
mknod $i p
touch $o

exec 7<> $i
exec 8<> $o

telnet $host 25 <&7  &

sleep 1
echo "helo arrow.archss.org" >> $i
echo "mail from:<test@arrow.archss.org>" >> $i
echo "rcpt to:<arrow@arrow.archss.org>" >> $i
echo "data" >> $i
echo "Hello, this is test mail data." >> $i
echo "" >> $i
echo "." >> $i
echo "quit" >> $i
