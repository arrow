ref:http://www.gentoo-cn.org/doc/zh_cn/git-howto.xml
基于git的Gentoo中文文档开发流程
内容:

1.  准备工作

安装git

注意: 请加上curl和tk USE标记。前者可以让你获取http协议的仓库，后者可以让你使用gitk工具。

代码 1.1: 安装git

$ euse -E curl tk
$ emerge -av dev-util/git

默认：安装和配置msmtp

代码 1.2: 安装msmtp

$ emerge -av msmtp

然后设置msmtp，修改~/.msmtprc并设定权限：

代码 1.3: 修改~/.msmtprc并设定权限

$ cat > ~/.msmtprc
defaults
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
account gmail
host smtp.gmail.com
from yourname@gmail.com
auth on
user yourname@gmail.com
password yourpassword
port 587
account default:gmail
$ chmod 600 ~/.msmtprc

请用你的email地址和密码代替上面的yourname@gmail.com和yourpassword。

备选：安装和配置ssmtp

代码 1.4: 安装ssmtp

$ emerge -av ssmtp

ssmtp的配置文件分别存储在/etc/ssmtp/ssmtp.conf和/etc/ssmtp/revaliases内。

代码 1.5: 修改/etc/ssmtp/ssmtp.conf

root=yourname@gmail.com
mailhub=smtp.gmail.com:587
rewriteDomain=
hostname=yourname@gmail.com
UseSTARTTLS=YES
AuthUser=yourname
AuthPass=yourpassword
FromLineOverride=YES

代码 1.6: 修改/etc/ssmtp/revaliases

root:yourname@gmail.com:smtp.gmail.com:587
user:yourname@gmail.com:smtp.gmail.com:587

请用你的email地址和密码代替上面的yourname@gmail.com和yourpassword，用你在的Gentoo Linux登录的用户名代替user。

配置git

安装好了smtp程序之后，我们来配置git本身。git的每个递交都会关联作者的email信息。在1.5.2.2版及以后的git中，可以使用git-config工具来设定git的一些全局变量。

代码 1.7: 设置自己的姓名、email地址及smtp服务器程序

$ git-config --global user.name 'Your Name'
$ git-config --global user.email 'yourname@gmail.com'
$ git-config --global sendemail.smtpserver "/usr/sbin/sendmail"

同样，用你自己的名字、email地址代替上面的Your Name和yourname@gmail.com。Gentoo里安装smtp服务器程序时都会附带安装一个/usr/sbin/sendmail符号连接，指向真正的smtp服务器程序可执行文件。所以直接把smtp服务器设定为此符号连接即可。

如此设置后，这些信息就会保存在~/.gitconfig文件中，作为用户个人的全局user相关设置。

获取主拷贝

开始工作前，首先在本地复制一个完整的主拷贝。

代码 1.8: 获取现有的主拷贝

$ cd /path/to/work
$ git clone http://www.gentoo-cn.org/git/gentoo-cn.git

命令成功以后，会创建一个gentoo-cn目录。里面的有个隐藏的.git目录，称为repository，包含了所有的历史数据。repository以外的其他文件称为工作拷贝（working copy），对其进行修改／删除不会影响repository的内容。Repository好比cvs/svn服务器中的内容，工作拷贝好比cvs /svn在客户端checkout出来的内容，可以随便修改。repository默认有一个分支（branch），叫master。当前分支的最新版本叫HEAD。和cvs/svn不同的是，基于git的开发是完全分布式的，没有一个集中的repository，任何开发者不但拥有工作拷贝，而且拥有所有的修改历史。这种开发方式给予开发者更多的信息和更大空间及自由度。

下一步请创建一个本地（local）分支并检出此分支。

代码 1.9: 创建并检出本地分支

$ cd gentoo-cn
$ git checkout -b local                   # 创建并检出分支
$ git branch                              # 显示分支
  master
* local                                          # 带*的为当前活动分支

注意: 以后所有的修改都请只在本地分支里进行。

这时可以先用gitk看看当前的修改历史。

代码 1.10: 使用gitk --all可以通过图形方式查看项目的所有分支结构和历史修改

$ gitk --all

2.  编辑修改

修改／添加／删除文件

完成上述准备工作后，你就可以开始你的正式工作了。你可以修改现有的文件，也可以添加新文件或删除文件。我们以添加一个文件为例，来看看如何利用git进行管理。

代码 2.1: 在工作目录中添加一个文件

$ echo "this is a test" > new_file.xml

检查所做的修改

你可以随时用git status和git diff查看目前的工作拷贝和repository HEAD的差别。

代码 2.2: 建立新文件后git status输出的结果

$ git status
# On branch local
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#       new_file.xml
nothing added to commit but untracked files present (use "git add" to track)

添加文件到repository

上述git status输出表明新文件new_file.xml还没有添加到repository。你可以用git add完成这个任务。对修改和删除的已有的文件不需要额外步骤。另外由于此文件是新加入的，现在使用git diff将不会显示任何结果。

代码 2.3: 添加文件

$ git add new_file.xml
$ git status
# On branch local
# Changes to be committed:
#   (use "git reset HEAD <file>..." to unstage)
#
#       new file:   new_file.xml
#

3.  提交修改

用git commit提交你的修改

你可以随时用git commit提交你的修改。刚开始使用git时，可以尽量多提交。因为和cvs/svn不同，这个拷贝是你个人的，你应该经常保存你的工作。你日后还可以对你的这些工作进一步整理，例如，合并多个提交结果，修改提交信息等，来形成最终可以发布到Gentoo China邮件列表的工作。

代码 3.1: 提交你的修改

$ git commit -a -s -m "added a new file"
Created commit 96ffadde5682bc9b3d96582822bc4e75e8dccdca
 1 files changed, 1 insertions(+), 0 deletions(-)
 create mode 100644 new_file.xml

和cvs/svn一样，你需要填写一个描述此次修改的简短信息。对于自己使用的临时提交，信息可以比较随便。

用gitk或者git diff查看新的历史

代码 3.2: 使用git diff

$ git diff HEAD^..HEAD
diff --git a/new_file.xml b/new_file.xml
new file mode 100644
index 0000000..0527e6b
--- /dev/null
+++ b/new_file.xml
@@ -0,0 +1 @@
+This is a test

git diff HEAD可以查看工作拷贝和HEAD的差别。git diff HEAD^..HEAD用于查看最后一次提交所作的修改。HEAD^表示最新版HEAD的上一个版本。类似的名称还有：HEAD^^，HEAD~2，local^^^，tag_name，提交对应SHA1哈希的前几位（用gitk或git whatchanged可以查看提交对应的哈希值）等等。请查看man git-rev-parse以获取更多有关信息。

4.  发布修改

整理你的个人提交

往往你需要对你的个人提交进行整理。尤其是当你想发布你的工作时，一般希望把之前的很多次提交集中起来。假设你还进行了如下的改动。

代码 4.1: 更多的修改

$ echo "this is the second line" >> new_file.xml
$ git diff
diff --git a/new_file.xml b/new_file.xml
index 90bfcb5..030e90b 100644
--- a/new_file.xml
+++ b/new_file.xml
@@ -1 +1,2 @@
 this is a test
+this is the second line
$ git commit -a -s -m "some more log"
Created commit 1958506a599d1bcecd38fb4879b3483545226ef4
 1 files changed, 1 insertions(+), 0 deletions(-)
$ git whatchanged HEAD^^..HEAD
commit 1958506a599d1bcecd38fb4879b3483545226ef4
Author: Xudong Guan <xudong.guan@gmail.com>
Date:   Fri Jul 13 16:44:07 2007 +0100

    some more log

:100644 100644 90bfcb5... 030e90b... M  new_file.xml

commit 96ffadde5682bc9b3d96582822bc4e75e8dccdca
Author: Xudong Guan <xudong.guan@gmail.com>
Date:   Fri Jul 13 16:28:08 2007 +0100

    some log

:000000 100644 0000000... 90bfcb5... A  new_file.xml

此时你希望将上两次修改的commit合二为一，再发布，这时可以通过如下的步骤。

代码 4.2: 整理你的上两个提交为一个新的提交

$ git reset --soft HEAD^^                 # 回退两个历史，但保留工作拷贝
$ git status
# On branch local
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#       new_file.xml
nothing added to commit but untracked files present (use "git add" to track)
$ git commit -a -m "Added new file new_file.xml
> 
> 张兄，这是一个新添加的文件，他的用途主要是演示
> 如何使用git.
> 
> 欢迎兄弟们使用。
> "

此时你现在的local分支保存了一个整理好的提交。同时写了一个更加详细的提交描述。现在建议用gitk --all看看两个分支的可视化效果。

由于本次提交需要发布，所以最好按照如下格式填写你的提交描述。

代码 4.3: 提交描述信息的格式

Added new file new_file.xml

张兄，这是一个新添加的文件，他的用途主要是演示
如何使用git.

欢迎兄弟们使用。

信息中的第一行简要描述此次修改的主题，它同时也会是将来发送到邮件列表上时的邮件主题。第一行后请空一行，然后象写邮件正文一行，详细描述你的修改。描述使用中文或英文都可以，但是标题使用中文的话，git将来生成提交的patch邮件文件名时，会把中文过滤掉。

发布你的修改

当你要发布你的工作时，你可以用git format-patch产生补丁并用git send-email将补丁发送到Gentoo China邮件列表（如果只是用于测试，可以发到一个个人的邮箱）。不过，与cvs/svn类似的是，补丁产生前之前应该先更新master分支，并且将本地分支rebase到master分支。rebase的概念类似于合并，但和git merge不完全一样。详情请查阅man git-rebase。

代码 4.4: 更新master分支且将本地分支rebase到master分支

$ git checkout master                     # 检出master
$ git pull                                # 更新master
$ git checkout local                      # 检出local
$ git rebase master                       # rebase到master

如果你在local分支的修改和master里的修改有冲突，这rebase就会中断，并且提示你哪个文件中发生了冲突以及你应该怎样做。此时你应该手工解决冲突，然后执行提示的命令。

代码 4.5: 解决冲突

$ vi new_file.xml                         # 解决文件中的冲突
$ git add new_file.xml                    # 标记为冲突已解决
$ git rebase --continue                   # 继续rebase

所有冲突解决完后，请用git format-patch自动生成要发布的邮件。

代码 4.6: 用git format-patch自动生成要发布的邮件

$ git format-patch master                 # 创建补丁
0001-Added-new-file-new_file.xml.patch

代码 4.7: 用git send-email发送邮件

$ git send-email 0001-Added-file-new_file_xml.patch --to gentoo-china@googlegroups.com

5.  接收补丁

维护人员／开发者接收你的补丁

你的邮件发到列表上后，会有一定的反馈意见。你在听取修改意见后可以再次发布。你的发布可以被维护人员接收到主拷贝。其他开发者也可以不通过主拷贝，而是通过邮件直接获得你的工作（me是用来加上接收人的Sign-off行的）。

首先你需要把补丁邮件接收到本地，并保存邮件全文到一个文件，比如tmp.txt。然后你可以使用git am。

代码 5.1: 接收他人工作

$ git am -s tmp.txt

假如打补丁失败，在修正补丁后，执行：

代码 5.2: 修正补丁之后

$ git am -s -r


本文档的内容遵循知识共享-署名-相同方式共享许可协议

	

打印

更新于2008年 3月 2日

本翻译的原始版本已经不再被维护

总结: 本文介绍了如何使用git来进行gentoo中文文档翻译工作的开发。本文的读者不需要有任何git基础。读完本文后，读者应该可以将自己的翻译或对现有工作的修改以补丁的形式发布到gentoo-china邮件列表上，供维护人员评阅和采纳。

管旭东
作者

张乐
编辑

程任全
贡献者

吴峰光
贡献者

江泽洲
贡献者

苏永恒
贡献者

井诚
贡献者

Donate to support our development efforts.
Gentoo Centric Hosting: vr.org

VR Hosted
Tek Alchemy

Tek Alchemy
SevenL.net

SevenL.net
Global Netoptex Inc.

Global Netoptex Inc.
Copyright 2001-2008 Gentoo Foundation, Inc. Questions, Comments? Contact us. 
