#
[ -z "$PS1" ] && return
export CVSROOT=:pserver:arrow_zhang@scs:12121/home/cvsadmin/sbu-iii
alias agvim="/usr/bin/gvim"
alias laptop="/usr/bin/ssh -X arrowup.3322.org"
alias goserver="/usr/bin/ssh -X sbu31"
alias go220="/usr/bin/ssh -X sbu31"
alias godell="/usr/bin/ssh -X dell"
alias startx="/usr/bin/startx >~/.x_startlog 2>&1" 
alias linux="/usr/bin/ssh -X linux"
alias gearrow="ge arrow"
alias geroot="sudo ge root"
alias debugarrow="telnet 192.168.1.1"
alias arrow-ping="ping www.163.com"
alias ccc='cvs -d :pserver:jeans@fog:/data/cvsroot'
alias env_english="export LANG=en_US.UTF-8; export LC_ALL=en_US.UTF-8"
alias env_chinese="export LANG=zh_CN.UTF-8; export LC_ALL=zh_CN.UTF-8"

alias tolap="`which ssh` -X 172.21.5.133 -p 12"
alias toarrow="/usr/bin/ssh -X 172.21.5.133"

alias exit="clear; exit"
alias pacsearch="yaourt -Sl | cut -d ' ' -f 2 | grep "
alias pacup="yaourt -Syu"
alias pac="yaourt -S"
alias pacs="pacsearch"
alias pacd="yaourt -Rs"
alias pacdf="yaourt -Rsn"
alias paci="yaourt -Si"
#pacsearch() {
#        echo -e "$(pacman -Ss "$@" | sed \
#        -e 's#core/.*#\\033[1;31m&\\033[0;37m#g' \
#        -e 's#extra/.*#\\033[0;32m&\\033[0;37m#g'\
#        -e 's#community/.*#\\033[1;35m&\\033[0;37m#g' \
#        -e 's#^.*/.* [0-9].*#\\033[0;36m^\\033[0;37m#g' ) \
#        \033[0m"
#}
#pacsearch() {
#   echo -e "$(pacman -Ss "$@" | sed \
#     -e 's#core/.*#\\033[1;31m&\\033[0;37m#g' \
#     -e 's#extra/.*#\\033[0;32m&\\033[0;37m#g' \
#     -e 's#community/.*#\\033[1;35m&\\033[0;37m#g' \
#     -e 's#^.*/.* [0-9].*#\\033[0;36m&\\033[0;37m#g' ) \
#     \033[0m"
#}

#    alias notes="wine /home/arrow/.wine/drive_c/Lotus/Notes/notes.exe"
#alias notes="export LANG=zh_CN.UTF-8;export LANGUAGE=zh_CN.UTF-8;LC_ALL=zh_CN.UTF-8;wine /home/arrow/.wine/drive_c/Lotus/Notes/notes.exe"
#alias notes="export LANG=zh_CN.GB2312;export LANGUAGE=zh_CN.GB2312;LC_ALL=zh_CN.GB2312;wine /home/arrow/.wine/drive_c/Lotus/Notes/notes.exe"
#alias notes="wine /home/arrow/.wine/drive_c/Lotus/Notes/notes.exe"
#alias stock="wine /home/arrow/.wine/drive_c/new_zxzq_test/TdxW.exe"
alias cdhome="cd \`cat /home/arrow/.tmp/.arrow-new-pwd\`"
alias mutt="LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 mutt"
alias x="startx & sleep 2 && exit"
alias e8test="export LANG=zh_CN.GB2312;export LANGUAGE=zh_CN.GB2312;LC_ALL=zh_CN.GB2312;wine /home/arrow/.wine/drive_c/e8test/e8test.exe"
alias qq="sudo /home/arrow/.bin/pro/qq/lumaqq"
alias vimlist="vim --serverlist"
alias ll='ls -l'
alias cd..='cd ..'
alias lf='/bin/ls --full-time'
alias la='ls -A'
alias l='ls -CF'
alias c="urxvtc"
alias grep="grep --color"
alias mycvs="cvs -d ${LAPCVSROOT} $*"

alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
#        alias cu='sudo chmod 777 /dev/ttyUSB0 && sudo cu -l /dev/ttyUSB0 -s 115200'
alias e="emacsclient"
alias ssh="/usr/bin/ssh -X"

XDM_PATH=/bin:/usr/bin:/sbin:/usr/sbin:/opt/kde/bin:/usr/bin/perlbin/site:/usr/bin/perlbin/vendor:/usr/bin/perlbin/core:/opt/qt/bin
ARROW_PATH=/home/arrow/.bin/shell:/usr/local/bin:/usr/local/sbin:/bin:/usr/games
A_TOOL_PATH=/opt/buildroot-gdb/bin:/opt/surfmilo/cross-dev/usr/bin
#A_TOOL_PATH=/work/.opt/mv_tools/bin
export PATH=$ARROW_PATH:$A_TOOL_PATH:$XDM_PATH

export MANPATH=$MANPATH:/work/.opt/iad6358/man
export EDITOR=vim

export MAC_SYS="00:90:F5:25:19:C1"
export SVNTP="svn://172.31.2.251:36901/svn/Platform/DG834GV/Neutral"
export SVNLINUX="https://linuxsys.googlecode.com/svn/trunk"
export SIP_DOMAIN=localhost
export XTERM=rxvt
export XTERMCMD=rxvt
export MAIL="~/Maildir"
export MAILCHECK="7200"
export SCREENDIR="${HOME}/.screen"

export PS1="[01;36m\u@[02;33m\H:\W> [0m"
#export PS1="\015\033[1m\u@\H:\W> \033[0m"
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi
if [ "$TERM" != "dumb" ]; then
    #arrow eval "`dircolors ~/.arrowcolor`"
    eval "`dircolors ~/.arrowcolor`"
    alias ls='ls --color=auto'
fi
shopt -s checkwinsize
if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
fi

case "$TERM" in
rxvt*|xterm*)
    PROMPT_COMMAND='echo -ne "\033]0;${PWD/$HOME/~} /-/${USER}@${HOSTNAME}\007|"'
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\007"'
    ;;
screen*)
    ;;
*)
    ;;
esac

case "$TERM" in
rxvt* | "rxvt-unicode") #not tty
	if [ "$A_HV_RT" = "Y" ]; then
		PWDFILE="${HOME}/.tmp/.arrow-new-pwd"
		if [ ! -f $PWDFILE ]; then
			break
		fi
		cd `cat $PWDFILE`
		unset A_HV_RT
	fi
	;;
esac
#fortune
#PS1='[01;32m\u[01;35m@[01;36m\h[00m:[01;34m \w\n[01;39m$?[01;38m\$ [00m'
#PS1='[01;32m\u[01;35m@[01;36m\h[00m:[01;34m \w[00m\n$?\$ '
PS1='[01;32m\u[01;35m@[01;36m\h[00m:[01;32m \w[00m \n$?\$ '

#---------
set +h
umask 022
CLFS=/mnt/clfs
#LC_ALL=POSIX
PATH=/cross-tools/bin:/bin:/usr/bin:~/.bin/shell:/usr/sbin/:/sbin
#PATH=/cross-tools/bin:/bin:/usr/bin
export CLFS LC_ALL PATH
export CLFS_HOST="i686-cross-linux-gnu"
export CLFS_TARGET="i686-linux-uclibc"

