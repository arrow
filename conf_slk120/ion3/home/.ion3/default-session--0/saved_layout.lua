-- This file has been generated by Ion. Do not edit.
return {
    [0] = {
        ["type"] = "WRootWin",
        ["name"] = "WRootWin",
        ["managed"] = {
            [1] = {
                ["type"] = "WGroupWS",
                ["name"] = "cmd_1",
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["switchto"] = true,
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["tls"] = 749,
                            ["tl"] = {
                                ["regparams"] = {
                                    ["mode"] = 1,
                                    ["type"] = "WFrame",
                                    ["name"] = "WFrame<1>",
                                    ["managed"] = {
                                        [1] = {
                                            ["type"] = "WGroupCW",
                                            ["name"] = "WGroupCW<3>",
                                            ["geom"] = {
                                                ["y"] = 23,
                                                ["x"] = 1,
                                                ["w"] = 1022,
                                                ["h"] = 725,
                                            },
                                            ["switchto"] = true,
                                            ["sizepolicy"] = "full",
                                            ["managed"] = {
                                                [1] = {
                                                    ["type"] = "WClientWin",
                                                    ["bottom"] = true,
                                                    ["windowid"] = 6291462,
                                                    ["checkcode"] = 1,
                                                    ["sizepolicy"] = "full",
                                                    ["geom"] = {
                                                        ["y"] = 0,
                                                        ["x"] = 4,
                                                        ["w"] = 1015,
                                                        ["h"] = 724,
                                                    },
                                                    ["level"] = 1,
                                                },
                                            },
                                            ["level"] = 0,
                                        },
                                        [2] = {
                                            ["type"] = "WGroupCW",
                                            ["name"] = "WGroupCW<4>",
                                            ["hidden"] = true,
                                            ["geom"] = {
                                                ["y"] = 23,
                                                ["x"] = 1,
                                                ["w"] = 1022,
                                                ["h"] = 725,
                                            },
                                            ["sizepolicy"] = "full",
                                            ["managed"] = {
                                                [1] = {
                                                    ["type"] = "WClientWin",
                                                    ["bottom"] = true,
                                                    ["windowid"] = 16777222,
                                                    ["checkcode"] = 2,
                                                    ["sizepolicy"] = "full",
                                                    ["geom"] = {
                                                        ["y"] = 0,
                                                        ["x"] = 4,
                                                        ["w"] = 1015,
                                                        ["h"] = 724,
                                                    },
                                                    ["level"] = 1,
                                                },
                                            },
                                            ["level"] = 0,
                                        },
                                        [3] = {
                                            ["type"] = "WGroupCW",
                                            ["name"] = "WGroupCW<1>",
                                            ["hidden"] = true,
                                            ["geom"] = {
                                                ["y"] = 23,
                                                ["x"] = 1,
                                                ["w"] = 1022,
                                                ["h"] = 725,
                                            },
                                            ["sizepolicy"] = "full",
                                            ["managed"] = {
                                                [1] = {
                                                    ["type"] = "WClientWin",
                                                    ["windowid"] = 20971542,
                                                    ["checkcode"] = 3,
                                                    ["level"] = 1,
                                                    ["geom"] = {
                                                        ["y"] = 0,
                                                        ["x"] = 0,
                                                        ["w"] = 1022,
                                                        ["h"] = 725,
                                                    },
                                                    ["bottom"] = true,
                                                },
                                            },
                                            ["level"] = 0,
                                        },
                                    },
                                },
                                ["type"] = "WSplitRegion",
                            },
                            ["dir"] = "vertical",
                            ["brs"] = 19,
                            ["br"] = {
                                ["type"] = "WSplitST",
                            },
                            ["type"] = "WSplitSplit",
                        },
                    },
                },
                ["level"] = 0,
            },
            [2] = {
                ["type"] = "WGroupWS",
                ["name"] = "cmd_2",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<2>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<8>",
                                ["managed"] = {
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [3] = {
                ["type"] = "WGroupWS",
                ["name"] = "ge(top)",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<4>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<3>",
                                ["managed"] = {
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [4] = {
                ["type"] = "WGroupWS",
                ["name"] = "firefox",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<5>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<7>",
                                ["managed"] = {
                                    [1] = {
                                        ["type"] = "WGroupCW",
                                        ["name"] = "WGroupCW",
                                        ["geom"] = {
                                            ["y"] = 23,
                                            ["x"] = 1,
                                            ["w"] = 1022,
                                            ["h"] = 744,
                                        },
                                        ["switchto"] = true,
                                        ["sizepolicy"] = "full",
                                        ["managed"] = {
                                            [1] = {
                                                ["type"] = "WClientWin",
                                                ["bottom"] = true,
                                                ["windowid"] = 12582981,
                                                ["checkcode"] = 4,
                                                ["sizepolicy"] = "full",
                                                ["geom"] = {
                                                    ["y"] = 0,
                                                    ["x"] = 0,
                                                    ["w"] = 1022,
                                                    ["h"] = 744,
                                                },
                                                ["level"] = 1,
                                            },
                                        },
                                        ["level"] = 0,
                                    },
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [5] = {
                ["type"] = "WGroupWS",
                ["name"] = "xpdf",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<6>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<9>",
                                ["managed"] = {
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [6] = {
                ["type"] = "WGroupWS",
                ["name"] = "tuxcmd",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<7>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<11>",
                                ["managed"] = {
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [7] = {
                ["type"] = "WGroupWS",
                ["name"] = "play",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<8>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<13>",
                                ["managed"] = {
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [8] = {
                ["type"] = "WGroupWS",
                ["name"] = "stardict",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<9>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<15>",
                                ["managed"] = {
                                    [1] = {
                                        ["type"] = "WGroupCW",
                                        ["name"] = "WGroupCW<2>",
                                        ["geom"] = {
                                            ["y"] = 23,
                                            ["x"] = 1,
                                            ["w"] = 1022,
                                            ["h"] = 744,
                                        },
                                        ["switchto"] = true,
                                        ["sizepolicy"] = "full",
                                        ["managed"] = {
                                            [1] = {
                                                ["type"] = "WClientWin",
                                                ["bottom"] = true,
                                                ["windowid"] = 14680140,
                                                ["checkcode"] = 5,
                                                ["sizepolicy"] = "full",
                                                ["geom"] = {
                                                    ["y"] = 0,
                                                    ["x"] = 0,
                                                    ["w"] = 1022,
                                                    ["h"] = 744,
                                                },
                                                ["level"] = 1,
                                            },
                                        },
                                        ["level"] = 0,
                                    },
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [9] = {
                ["type"] = "WGroupWS",
                ["name"] = "wine",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<10>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<6>",
                                ["managed"] = {
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [10] = {
                ["type"] = "WGroupWS",
                ["name"] = "screen",
                ["hidden"] = true,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["type"] = "WTiling",
                        ["name"] = "WTiling<1>",
                        ["bottom"] = true,
                        ["level"] = 0,
                        ["sizepolicy"] = "full",
                        ["geom"] = {
                            ["y"] = 0,
                            ["x"] = 0,
                            ["w"] = 1024,
                            ["h"] = 768,
                        },
                        ["split_tree"] = {
                            ["regparams"] = {
                                ["mode"] = 1,
                                ["type"] = "WFrame",
                                ["name"] = "WFrame<2>",
                                ["managed"] = {
                                },
                            },
                            ["type"] = "WSplitRegion",
                        },
                    },
                },
                ["level"] = 0,
            },
            [11] = {
                ["type"] = "WGroupWS",
                ["sizepolicy"] = "full",
                ["managed"] = {
                    [1] = {
                        ["geom"] = {
                            ["y"] = 144,
                            ["x"] = 192,
                            ["w"] = 640,
                            ["h"] = 480,
                        },
                        ["type"] = "WFrame",
                        ["name"] = "*scratchpad*",
                        ["bottom"] = true,
                        ["level"] = 1025,
                        ["mode"] = 0,
                        ["managed"] = {
                        },
                        ["sizepolicy"] = "free_glue",
                    },
                },
                ["unnumbered"] = true,
                ["name"] = "*scratchws*",
                ["pseudomodal"] = true,
                ["level"] = 1,
                ["geom"] = {
                    ["y"] = 0,
                    ["x"] = 0,
                    ["w"] = 1024,
                    ["h"] = 768,
                },
                ["hidden"] = true,
            },
        },
    },
}

