
source /etc/arrowenv
if [ "$ETH_LAN" == "" ]; then
	/bin/echo "please set (ETH_LAN/ETH_WAN/ETH_WLAN, ARROW_HOST) first"
fi

source .bashrc

#export LANG=zh_CN.UTF-8
#export LC_ALL=zh_CN.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

if [ "$TERM" == "linux" ]; then
	export LANG=en_US;
	export LC_ALL=en_US;
fi

#if [[ -z "$DISPLAY" ]] && [[ $(tty) = /dev/vc/1 ]]; then
#        x
#        logout
#fi

