/* copyleft (C) GPL3 {{{2
 * Filename:	attr.c
 *
 * Author:	arrow <arrow_zhang@sdc.sercomm.com>
 * Created at:  Tue Jan  8 23:04:51 2008
 * }}}*/
/*header files	{{{1*/
#include <ncurses.h>
/*}}}*/

/*declaration		{{{1*/
/*}}}*/

/*functions		{{{1*/
int main(int argc, char *argv[])
{
	int ch, prev;
	FILE *fp;
	int goto_prev = FALSE, y, x;
	if (argc != 2) {
		printf("usage: %s<a c file name>\n", argv[0]);
		return 0;
	}
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		perror("cannot open input file");
		return 0;
	}
	initscr();
	prev = EOF;
	while ((ch = fgetc(fp)) != EOF) {
		if (prev == '/' && ch == '*') {
			attron(A_BOLD);
			goto_prev = TRUE;
		}
		if (goto_prev == TRUE) {
			getyx(stdscr, y, x);
			move(y, x - 1);
			printw("%c%c", '/', ch);
			ch = 'a';
			goto_prev = FALSE;
		} else {
			printw("%c", ch);
		}
		refresh();
		if (prev == '*' && ch == '/') {
			attroff(A_BOLD);
		}
		prev = ch;
	}
	getch();
	endwin();
	return 0;
}

/* vim:fdm=marker:ts=8:ft=c:norl:fdl=1:
 * }}}*/

