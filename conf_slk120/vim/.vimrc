"Author:arrow, for archlinux

if v:progname =~? "evim"
	finish
endif
set nocompatible
set backupdir=~/.tmp,/tmp,./
set backup		" keep a backup file
set history=100		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
map Q gq
map t gt 
map T gT 
set encoding=UTF-8
set report=0
set ignorecase
set nowrap
set incsearch
filetype plugin indent on
set autoindent
set tabstop=8
set softtabstop=8
set shiftwidth=8
set cinoptions=>8,n-8,{8,^-8,:8,=8,g8,h8,p8,t8,+8,(8,u8,w1,m1 shiftwidth=8 tabstop=8
set noexpandtab
set cindent
set notagbsearch
set tagrelative
if &t_Co > 2 || has("gui_running")
	syntax on
	set hlsearch
endif
nmap <F11> :nohlsearch<CR>
"nmap <F3> :only<CR>
"nmap <F4> :set noscrollbind<CR>:set scrollopt=<CR>:set nowrap<CR>:set foldmethod=manual<CR>:set foldcolumn=0<CR>:only<CR>zR
"nmap <F5> :set tags-=/usr/include/tags<CR>
"nmap <F6> :set tags+=/usr/include/tags<CR>
"nmap <F7> :set tags+=/kernel/tags<CR>
"nmap <F8> :set tags-=/kernel/tags<CR>
set backspace=indent,eol,start
set nosmartindent
set formatoptions=croqlmM
set textwidth=80
set laststatus=2
set statusline=%((%1*%M%*%R%Y)%)%f%=%(\[%3l-%02c]%)[%03b/%02B]T9\ %P~%L
set tags=tags
if has("multi_byte")
	set fileencoding=utf-8
	set fileencodings=utf-8,chinese,ucs-bom
	set ambiwidth=double
endif

if has("autocmd")
	filetype plugin indent on
	autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\   exe "normal g`\"" |
	\ endif
	set formatprg=fmt
	au FileType c set formatprg=indent
	au FileType cpp set formatprg=indent
	if !exists("auto_arrow_c")
	let auto_arrow_c=1
		au BufNewFile *.c 0r ~/.vim/files/c.skel
		"au BufNewFile *.c normal gnp 
		au BufNewFile ver.h 0r ~/.vim/files/ver.skel
		au BufNewFile *.h 0r ~/.vim/files/h.skel
		au BufNewFile *.sh 0r ~/.vim/files/sh.skel
		au BufNewFile *.txt 0r ~/.vim/files/txt.skel
		au BufNewFile Makefile 0r ~/.vim/files/makefile.skel
		au BufNewFile Rules.make 0r ~/.vim/files/Rules.make.skel
		":%s/_filename_/\=bufname("%")
		:"%s/_datetime_/\=strftime("%c")
		map gse <ESC>:%s/_filename_/\=bufname("%")/<CR>:%s/_datetime_/\=strftime("%c")/<CR> 
		au BufNewFile *.[ch] normal gse
		au BufNewFile *.sh normal gse
		"au BufNewFile *.txt normal gse
	endif
	au BufRead,BufNewFile *.viki set ft=viki
endif
if has("gui_running")
	"set menu
	:source $VIMRUNTIME/delmenu.vim
	:source $VIMRUNTIME/lang/menu_en_gb.utf-8.vim
	:source $VIMRUNTIME/menu.vim
	set foldcolumn=0
	set guioptions=aegilLtb
	"colorscheme delek
	set guifont=Courier\ 10
else
	"colorscheme  default
endif
colorscheme  default
set mouse=a
:language time C
":language ctype C
:language messages C
set showtabline=2
hi IncSearch term=reverse cterm=reverse gui=reverse
hi Search term=reverse ctermbg=darkmagenta ctermfg=white guibg=darkmagenta guifg=white
hi Cursor gui=reverse guifg=darkcyan guibg=white
"
let g:vikiUseParentSuffix=1
let g:vikiOpenUrlWith_mailto = 'mail %{URL}'
let g:vikiOpenFileWith_html  = "silent !firefox %{FILE}"
let g:vikiOpenFileWith_ANY   = "silent !ge top %{FILE}"
"fun! ConvertPDF()
"	if !exists("b:convertedPDF")
"		exec "cd ". expand("%:p:h")
"		exec "%!pdftotext ". expand("%:t") ." -"
"		:%!par 72w
"		cd -
"		setlocal noswapfile buftype=nowrite
"		let b:convertedPDF = 1
"	endif
"endf
"let g:vikiOpenFileWith_pdf = 'call VikiOpenLink("%{FILE}", "", 1)|silent call ConvertPDF()'
let g:deplatePrg = "deplate -x -X "
au FileType viki compiler deplate
"let g:vikiNameSuffix=".viki"
"autocmd! BufRead,BufNewFile *.viki set filetype=viki
"autocmd! BufRead,BufNewFile $HOME/viki/* set filetype=viki
set mousehide " Hide the mouse when typing text
set mousemodel=extend
set path=.,include,/usr/include,,
set wildmenu
set wildmode=list:full
let g:vimwiki_home = "~/wiki/"
