 精通initramfs构建step by step （七）：modules

二十二、内核模块支持
到目前为止，我们在构建initramfs时还没有涉及内核模块的支持，所用到的硬件驱动程序都是直接编译到内核中。现在我们就看看如何使initramfs支持内核模块。
首先，内核配置要支持模块，并支持内核模块的自动加载功能：在内核配置菜单中的激活下面的配置项，编译进内核 Load module support / Enable loadable module support / Automatic kernel loading ；
然后把需要的硬件驱动程序配置模块形式，比如把我的机器上的硬盘控制器的驱动编译成模块，则选择
Device Driver
|---->SCSI device support
|---->SCSI disk support
|----->verbose SCSI error reporting (不是必须的，但可方便问题定位)
|----->SCSI low-level drivers
  |---->Serial ATA (SATA) support
  |---->intel PIIX/ICH SATA support
把它们配置成模块。
最后，编译内核，并把编译好的内核模块安装到image的目录下：
make
make INSTALL_MOD_PATH=~/initramfs-test/image modules_install
命令执行完毕后，在image/lib/modules/2.6.17.13/kernel/drivers/scsi目录下安装了4个内核模文件：scsi_mod.ko、sd_mod.ko、ata_piix.ko、libata.ko，它们就是所需的硬盘控制器的驱动程序。

好了，都准备好了，可以用cpio命令生成inintramfs了。不过，为了方便后面的试验，我们再把init脚本改成
#!/bin/sh
mount -t proc proc /proc
mount -t sysfs sysfs /sys
mdev -s
exec /bin/sh
使系统启动后进入shell环境，并且用exec调用的方式，使shell的pid为1，能够执行switch_root命令。

二十三、试验：用initramfs中的内核模块安装硬盘文件系统
用新生成的initramfs启动系统，内核并没有自动加载硬盘控制器的驱动程序，所以 /dev目录下也没有sda等硬盘设备文件。好吧，我们自己加载内核模块文件。不幸的是，busybox的modprobe命令执行不正常，不能加载内核模块。怀疑是busybox的modprobe命令配置或编译有问题，后续再花时间定位吧，先用insmod命令依次加载。查看/lib/modules /2.6.17.13/modules.dep，弄清楚了4个模块的依赖关系，执行下面的命令加载：
insmod scsi_mod
insmod libata
insmod ata_piix
insmod sd_mod
然后再用
mdev -s
命令生成硬盘的设备文件。
好了，可以安装CLFS的硬盘分区，并把根文件系统切换到CLFS的硬盘分区：
mount /dev/sda8 /mnt
exec switch_root /mnt /sbin/init  
系统正常启动到了CLFS，我们可以做到用initramfs中的硬盘控制器的驱动模块安装硬盘分区了。

二十四、mdev的hotplug模式
上面的试验中，我们在加载完驱动模块后调用了mdev -s 命令来生成硬盘的设备文件。其实，可以使用mdev的hotplug模式在加载内核时自动生成对应的设备文件：
在执行insmod命令前，用
echo /sbin/mdev > /proc/sys/kernel/hotplug
命令设置系统的hotplug程序为mdev。
后续使用insmod命令加载模块时，系统自动调用mdev生成相应的设备文件。
注意：内核必须配置支持hotplug功能，而前面提到的CLFS最简内核配置方案是没有配置hotplug支持的。


－－－下节预告－－－
在这节里我们知道自己的硬件情况，也知道加载什么样的内核模块，所以用了一系列insmod命令加载硬件驱动程序。如果要做一个支持各种硬件配置的 initramfs，就不能使用这种方法，而是要借助著名的udev来自动根据内核侦测到硬件类型来加载相应的驱动程序，也就是系统的coldplug。在initramfs中如何使用udev做coldplug，请看下一个step：
精通initramfs构建step by step （八）：coldplug
